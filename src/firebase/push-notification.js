import firebase from "firebase"

firebase.initializeApp({
   apiKey: "",
   messagingSenderId: "",
   projectId: "",
   appId: "",
})
const messaging = firebase.messaging()
if ("serviceWorker" in navigator) {
   window.addEventListener("load", async () => {
      const registration = await navigator.serviceWorker.register(
         "/firebase-messaging-sw.js",
         {
            updateViaCache: "none",
         }
      )
      messaging.useServiceWorker(registration)
      messaging.onMessage((payload) => {
         console.log("Message payload", payload)
         const title = payload.notification.title
         const image = payload.notification.image
         const options = {
            body: payload.notification.body,
            image,
         }
         registration.showNotification(title, options)
      })
   })
}
export const askForPermissioToReceiveNotifications = async () => {
   try {
      const messaging = firebase.messaging()
      const token = await messaging.getToken()
      console.log("token: ", token)
      return token
   } catch (error) {
      console.error(error)
   }
}
