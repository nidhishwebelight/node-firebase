import { askForPermissioToReceiveNotifications } from "./firebase/push-notification"

function App() {
   return (
      <div>
         <h3>React-Firebase App</h3>
         <button onClick={askForPermissioToReceiveNotifications}>
            Click here
         </button>
      </div>
   )
}

export default App
